from external_sort import merge_sort


def main():
    input_file = 'input.txt'

    merge_sort(input_file)


if __name__ == '__main__':
    main()
